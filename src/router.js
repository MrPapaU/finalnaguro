import Vue from 'vue'
import Router from 'vue-router'
import Login from './views/Login.vue'
import Signup from './views/Signup.vue'
import Home from './views/Home.vue'
import Feedback from './views/Feedback.vue'
import Root from './views/Root.vue'
import Chapter from './views/Chapter.vue'
import Example from './views/Example.vue'
import Quiz from './views/Quiz.vue'

Vue.use(Router)

const router = new Router({
    mode: 'history',
    routes: [{
        path: '/login',
        component: Login
    },{
        path: '/signup',
        component: Signup
    },
    {
        path: '/home',
        component: Home
    },
    {
        path: '/root',
        component: Root
    },
    {
        path: '/chapter',
        name: 'chapter',
        component: Chapter
      },
      {
        path: '/example',
        name: 'example',
        component: Example
      },
      {
        path: '/quiz',
        name: 'quiz',
        component: Quiz
      },
    {
        path: '/feedback',
        component: Feedback
    }]
})

export default router;