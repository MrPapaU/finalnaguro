from django.urls import include, path

urlpatterns = [
    path('users/', include('users.urls')),
    path('rest-auth/login', include('rest_auth.login.urls')),
    path('rest-auth/registration/', include('rest_auth.registration.urls')),
]